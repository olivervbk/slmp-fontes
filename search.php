<?php
	require_once("include/session.inc");
	require_once("include/functions.inc");

	include("include/header.inc");
?>

<?php
$busca= "";
if(isset($_POST['search'])){
	$busca = $_POST['search'];

}elseif(isset ($_GET['search'])){
	$busca = $_GET['search'];
	$_POST['tipo'] = "nome";
}
?>

<form action="search.php" method="POST">
<div>
	<input type="text" name="search" value="<?php echo $busca;?>" >
	<input type="submit" value="BUSCAR">
	<input type="button" value="FECHAR" onClick="window.close();">
</div><br>
<b>Procurar por:</b><br>
<?php
	$tipos = array(
		"assunto" => "Assunto",
		"centro" => "Centro",
		"cargo" => "Cargo",
		"nome" => "Nome",
		"todos" => "Todos",
		);
	$mtipo = "nome";
	if(isset($_POST['tipo']) && array_key_exists($_POST['tipo'], $tipos)){
		$mtipo = $_POST['tipo'];
	}
	foreach ($tipos as $index => $text){
		echo "<input type='radio' name='tipo'";
		if($mtipo == $index){
			echo " CHECKED='yes'";
		}
		echo " value='".$index."'>".$text."<br>\n";
	}
?>
</form>

<?php
if(strlen($busca) < 4){
	printStatus("error", "Procuras com mais de 3 caracteres, por favor.");
	$busca = "";
}

if($busca !== ""){
	$tipo = 0;
	if(isset($_POST['tipo'])){
		$tipo=$_POST['tipo'];

		if($tipo == "nome"){
			$resultado = $db->buscaNome($busca);
		}elseif($tipo == "centro"){
			$resultado = $db->buscaCentro($busca);
		}elseif($tipo == "assunto"){
			$resultado = $db->buscaTema($busca);
		}elseif($tipo == "cargo"){
			$resultado = $db->buscaCargo($busca);
		}elseif($tipo == "todos"){
			$resultado = $db->buscaTodos($busca);
		}else{
			printStatus("error", "tipo desconhecido!");
		}
	}
	if(isset($resultado)){
?>
<input type="button" value="Excluir Selecionado" onClick="parentDoExclusion();">
<?php echo "<b>Resultados:</b> ".count($resultado)."<br><br>\n";?>
<?php printStatus("info", "Clique na linha para atualizar na janela principal."); ?>
<hr>
<table>
	<tr>
		<th>NOME</th>
		<th>CARGO</th>
		<th>CENTRO</th>
	</tr>
<?php
	foreach ($resultado as $row){
	echo "<tr id='".$row["Id"]."' onmouseover=\"this.style.cursor = 'hand';\" onClick=\"setSelectedResult(this);\">\n";
	echo "<td>".$row["Nome"]."</td>\n";
	echo "<td>".$row["Cargo"]."</td>\n";
	echo "<td>".$row["Centro"]."</td>\n";
	echo "</tr>\n";
	
	}
?>

</table>

<?php
	}//if isset resultado
}//if ! empty busca
?>


<?php
	include("include/footer.inc");
?>
