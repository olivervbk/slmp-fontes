<?php
	require_once("include/session.inc");
	require_once("include/functions.inc");

	include("include/header.inc");
?>

<?php
$enable_exlude=false;
if(isset($_GET['action']) && $_GET['action'] == "manage"){
	$enable_exclude=true;
	if(isset($_POST['nome']) && isset($_POST['submit']) && $_POST['submit'] == "Novo"){
		if($db->inserirTema($_POST['nome']) == -1){
			printStatus("error", "Erro ao inserir tema!");
		}else{
			printStatus("ok", "Tema adicionado com sucesso.");
		}
	}
	if(isset($_GET['excluir'])){ 
		if($db->excluirTema($_GET['excluir']) == -1){
			printStatus("error", "Erro ao excluir tema!");
		}else{
			printStatus("ok", "Tema excluido com sucesso.");
		}
	}
?>
<h3>Gerenciar assuntos</h3>
<form action="themes.php?action=manage" method="POST">
<input type="text" name="nome"><input type="submit" value="Novo" name="submit"><br>
<input type="button" value="Voltar" onClick="window.location='themes.php';">
</form>

	
<?php	
}else{
?>

<h3>Adicionar assunto a fonte</h3>
<input type="button" value="Adicionar" onClick="parentAddTheme();">
<input type="button" value="Fechar" onClick="window.close();">
<input type="button" value="Gerenciar" onClick="window.location+='?action=manage';">

<?php
}

$temas = $db->listarTemas();
if($temas === -1){
	printStatus("error", "Erro ao listar temas!");
}else{
?>

<hr>
<div style="height:85%;width:100%;overflow:scroll;border: outline;">
<table>

<?php
	foreach ($temas as $row){
		echo "<tr id='".$row["Assuntos"]."' onmouseover=\"this.style.cursor = 'hand';\" onClick=\"setSelectedTheme(this);\">\n";
		echo "<td>".$row["Assuntos"]."</td>\n";
		if($enable_exclude){
			echo "<td><a href='themes.php?action=manage&excluir=".$row["Assuntos"]."'>[EXCLUIR]</a></td>\n";
		}
		echo "</tr>\n";
	}
?>

</table>
</div>

<?php
}//if isset resultado
?>


<?php
	include("include/footer.inc");
?>
