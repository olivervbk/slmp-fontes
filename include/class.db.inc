<?php

include ("config.inc");

class Database {
	private $mysqli;
	
	function __construct(){
		global $DB_HOST;
		global $DB_USER;
		global $DB_PASS;
		global $DB_NAME;
		
		$this->mysqli = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);
		if(!$this->mysqli){
			die("Could not connect to DB!");
		}
		$this->mysqli->set_charset("latin1");
		//if(!mysql_set_charset("latin1")){
		//	die("Could not set charset!");
		//}
	}

	public function close(){
		$this->mysqli->close();
	}

	//lista os temas
	public function listarTemas(){
		$result = $this->mysqli->query("SELECT Assuntos FROM fontassunto ORDER BY Assuntos");

		if(!$result)
			return -1;
		$return	= array();
		while($row = $result->fetch_assoc())
		{
			array_push($return, $row);
		}
		return $return;
	}

	private function busca($genero, $termo){
		$termos = split(" ", $termo);
		$sql = "SELECT Nome, Cargo, Centro, Temas, Telefones, Email, Historico, Id FROM fontes WHERE";
		$typeDef = '';
		foreach($termos as $key=>$termo){
			$termos[$key] = '%'.$termo.'%';
			$sql .= " $genero LIKE ?";
			$typeDef .= 's';
			if(count($termos) != $key+1){
				$sql .= " AND";
			}
		}
		$stmt = $this->mysqli->prepare($sql);

		$method = new ReflectionMethod('mysqli_stmt', 'bind_param');  //allows for call to mysqli_stmt->bind_param using variable argument list       
		$bindParamsReferences = array();  //will act as arguments list for mysqli_stmt->bind_param  

		foreach($termos as $key => $value){
			$bindParamsReferences[$key] = &$termos[$key];  
		}

		array_unshift($bindParamsReferences,$typeDef); //returns typeDefinition as the first element of the string  
		$method->invokeArgs($stmt,$bindParamsReferences); //calls mysqli_stmt->bind_param suing $bindParamsRereferences as the argument list

#error_log($sql);
		$stmt->execute();
		$stmt->bind_result($nome, $cargo, $centro, $temas, $telefones, $email, $historico, $id);
		$return	= array();
		while($stmt->fetch())
		{
			$row = array();
			$row["Nome"] = $nome;
			$row["Cargo"] = $cargo;
			$row["Centro"] = $centro;
			$row["Temas"] = $telefones;
			$row["Telefones"] = $telefones;
			$row["Email"] = $email;
			$row["Historico"] = $historico;
			$row["Id"] = $id;
			array_push($return, $row);
		}
		return $return;
	}

	//busca pelo nome
	public function buscaNome($nome){
		return $this->busca("Nome", $nome);
	}
	
	//busca pelo centro
	public function buscaCentro($centro){
		return $this->busca("Centro", $centro);
	}
	
	//busca pelo cargo
	public function buscaCargo($cargo){
		return $this->busca("Cargo", $cargo);
	}
	
	//busca pelo tema
	public function buscaTema($tema){
		return $this->busca("Temas", $tema);
	}

	public function buscaTodos($termo){
		$campos = array("Nome", "Cargo", "Centro", "Temas", "Historico");
		$termos = split(" ", $termo);
		$sql = "SELECT Nome, Cargo, Centro, Temas, Telefones, Email, Historico, Id FROM fontes WHERE";
		$typeDef = '';
		foreach ($campos as $icampo => $campo){
			$sql .= " ( ";
			foreach($termos as $itermo=>$termo){
				$termos[$itermo] = '%'.$termo.'%';

				$sql .= " $campo LIKE ?";
				$typeDef .= 's';
				if(count($termos) != $itermo+1){
					$sql .= " AND";
				}
			}
			$sql .= " ) ";
			if(count($campos) != $icampo+1){
				$sql .= " OR ";
			}
		}
		$stmt = $this->mysqli->prepare($sql);

		$method = new ReflectionMethod('mysqli_stmt', 'bind_param');  //allows for call to mysqli_stmt->bind_param using variable argument list       
		$bindParamsReferences = array();  //will act as arguments list for mysqli_stmt->bind_param  

		$val = 0;
		foreach($campos as $icampo => $campo){
			foreach($termos as $itermo => $termo){
				$bindParamsReferences[$val] = &$termos[$itermo];  
				$val++ ;
			}
		}

		array_unshift($bindParamsReferences,$typeDef); //returns typeDefinition as the first element of the string  
		$method->invokeArgs($stmt,$bindParamsReferences); //calls mysqli_stmt->bind_param suing $bindParamsRereferences as the argument list

#error_log($sql);
		$stmt->execute();
		$stmt->bind_result($nome, $cargo, $centro, $temas, $telefones, $email, $historico, $id);
		$return	= array();
		while($stmt->fetch())
		{
			$row = array();
			$row["Nome"] = $nome;
			$row["Cargo"] = $cargo;
			$row["Centro"] = $centro;
			$row["Temas"] = $telefones;
			$row["Telefones"] = $telefones;
			$row["Email"] = $email;
			$row["Historico"] = $historico;
			$row["Id"] = $id;
			array_push($return, $row);
		}
		return $return;
	}
	
	//número de entradas no banco
	public function tamanho(){
		$result = $this->mysqli->query("SELECT COUNT(*) AS tamanho FROM fontes");
		if(!$result)
			return -1;
		$ret = $result->fetch_assoc();
		return $ret["tamanho"]-1;
	}
	
	//elemento da posição INDEX
	public function get($index){
		if(!is_numeric($index)) return -1;
		if($index < 0)
			return null;

		$result = $this->mysqli->query("SELECT Nome, Cargo, Centro, Temas, Telefones, Email, Historico, Id FROM fontes ORDER BY Nome LIMIT $index,1");
		if(!$result){
			print "error fetching $index<br>";
			return -1;
		}
		return $result->fetch_assoc();
	}
	
	//edita o elemento da posição INDEX
	public function editar($id, $nome, $cargo, $centro, $temas, $telefones, $email, $historico)
	{
		$stmt = $this->mysqli->prepare("UPDATE fontes SET Nome = ?, Cargo = ?, Centro = ?, Temas = ?, Telefones = ?, Email = ?, Historico = ? WHERE Id = ?");
		$stmt->bind_param('sssssssi', $nome, $cargo, $centro,$temas, $telefones, $email, $historico, $id);
		if(!$stmt->execute())
			return -1;
		return $this->getIndex($id);
	}
	
	//Adiciona um novo elemento
	public function inserir($nome, $cargo, $centro, $temas, $telefones, $email, $historico)
	{
		$stmt = $this->mysqli->prepare("INSERT INTO fontes (Nome, Cargo, Centro, Temas, Telefones, Email, Historico) VALUES(?,?,?,?,?,?,?)");
		$stmt->bind_param('sssssss', $nome, $cargo, $centro, $temas, $telefones, $email, $historico);
		if(!$stmt->execute())
			return -1;

		$id = $this->mysqli->insert_id;
		return $this->getIndex($id);
	}
	
	//Excluir elemento
	public function excluir($id)
	{
		if(!is_numeric($id)){
			echo "is not int PORRA:$id";
			return -1;
		}
		$result = $this->mysqli->query("DELETE FROM fontes WHERE Id = $id");
		if(!$result){
			echo $this->mysqli->error;
			return -1;
		}
		return null;
	}
	
	//Adiciona um novo tema
	public function inserirTema($tema)
	{
		$stmt = $this->mysqli->prepare("INSERT INTO fontassunto (Assuntos) VALUES(?)");
		$stmt->bind_param('s', $tema);
		if(!$stmt->execute())
			return -1;
		return null;
	}
	
	//Excluir tema
	public function excluirTema($tema)
	{
		$stmt = $this->mysqli->prepare("DELETE FROM fontassunto WHERE Assuntos = ? LIMIT 1");
		$stmt->bind_param('s', $tema);
		if(!$stmt->execute())
		if(!$result){
			echo $this->mysqli->error;
			return -1;
		}
		return null;
	}
	
	//Converte o ID para o INDEX
	public function getIndex($id)
	{
		if(!is_numeric($id))
			return -1;
		$result = $this->mysqli->query("SELECT Num FROM (SELECT Nome, Id, (@row:=@row+1) as Num FROM fontes, (SELECT @row:=0)r ORDER BY Nome)a WHERE Id = $id");
		if(!$result)
			return -1;
		$return = $result->fetch_assoc();	
		return $return["Num"]-1;
	}
	
};

$db = new Database;
?>

