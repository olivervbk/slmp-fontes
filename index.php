<?php
	//SLMP? :D
	require_once("include/session.inc");
	require_once("include/functions.inc");

	include("include/header.inc");

$fields = array();
$fields['Nome'] = "";
$fields['Cargo'] = "";
$fields['Centro'] = "";
$fields['Telefones'] = "";
$fields['Email'] = "";
$fields['Historico'] = "";
$fields['Temas'] = "";

$tamanho = $db->tamanho();

$index=0;
if(isset($_POST['index'])){
	$index = $_POST['index'];
}
if(isset($_GET["refresh_with_id"])){
	$index = $db->getIndex($_GET["refresh_with_id"]);
}
if(isset($_GET["do_exclusion"])){
	$retCode = $db->excluir($_GET["do_exclusion"]);
	if($retCode == -1){
		printStatus("error", "Erro ao excluir!");
	}else{
		printStatus("ok", "excluido com sucesso");
		$index = 0;
	}
}

$last_action = "";
if(isset($_POST["last_action"])){
	$last_action = $_POST["last_action"];
}

$readonly="disabled";
$getData = true;
if(isset($_POST['action'])){
	$action = $_POST['action'];
	if($action == "NOVO"){
		printStatus("info", "insira as informa&ccedil;&otilde;es e pressione 'GRAVAR' para salvar ou 'EXCLUIR' para voltar.");
		//readonly = false
		$readonly = "";
	
		//erase fields
		$index = -1;

	}elseif($action == "GRAVAR"){

		$error = 0;
		if($last_action == "NOVO" || $last_action == "EDITAR"){
			$vars = array("nome"=>"Nome",
				 "cargo"=>"Cargo",
				 "centro"=>"Centro",
				 "temas"=>"Temas",
				 "telefones"=>"Telefones");
			//to_upper($vars[i][0]) ? =/
			foreach ($vars as $var=>$aux){
				if(!isset($_POST[$var]) || trim($_POST[$var]) == ""){
					printStatus("error", "Campo '$var' tem que ser definido!");
					$error++;
				}else{
					$fields[$aux] = $_POST[$var];
				}
			}
		}

		if(!$error){
			
			//novo
			if($last_action == "NOVO"){
				$retCode = $db->inserir(
					$_POST["nome"], 
					$_POST["cargo"],
					$_POST["centro"],
					$_POST["temas"],
					$_POST["telefones"],
					$_POST["email"],
					$_POST["historico"]
				);
				if($retCode < 0){
					printStatus("error", "Erro ao inserir!");
				}else{
					print "NOVO:got:".$retCode."<br>\n";
					$index = $retCode;
				}
					
			}elseif($last_action == "EDITAR"){
				$retCode = $db->editar(
					$_POST["id"],
					$_POST["nome"], 
					$_POST["cargo"],
					$_POST["centro"],
					$_POST["temas"],
					$_POST["telefones"],
					$_POST["email"],
					$_POST["historico"]
				);
				if($retCode < 0){
					printStatus("error", "Erro ao editar!");
				}else{
					print "EDITAR:got:".$retCode."<br>\n";
					$index = $retCode;
				}
			}else{
				printStatus("error", "Ultima acao nao foi NOVO ou EDITAR!");
			}

		}else{
			$action=$last_action;
			$readonly="";
		}

	}elseif($action == "EDITAR"){
		if($last_action == "NOVO"){
			printStatus("error", "Impossivel editar um item a ser criado!");
			$action = "NOVO";
		}
		printStatus("info", "modifique as informa&ccedil;&otilde;es e pressione 'GRAVAR' para salvar ou 'EXCLUIR' para cancelar.");
		$readonly = "";

	}elseif($action == "DESCARTAR"){
		//check if canceling NOVO
		if($last_action == "NOVO"){
			$index = 0;
		}
	}elseif($action == "EXCLUIR"){
		//remove from db
		$retCode = $db->excluir($_POST["id"]);
		if($retCode == -1){
			printStatus("error", "Erro ao excluir!");
		}else{
			printStatus("ok", "excluido com sucesso");
			$index = 0;
		}

	}elseif($action == "<<"){
		$index = 0;

	}elseif($action == "<"){
		if($index > 0){
			$index--;
		}else{
			$index = 0;
		}

	}elseif($action == ">"){
		if($index < $tamanho){
			$index++;
		}

	}elseif($action == ">>"){
		$index = $tamanho;

	}else{
		//invalid action...
		printStatus("error", "Acao invalida!");
	}
}

if($index != -1){
	$fields = $db->get($index);
}

echo "<!-- DEBUG: $index(".$fields["Id"].")<br> -->\n";
echo "<b>Numero de Fontes:</b> ".($tamanho+1)."<br>\n";
?>


<form action='index.php' method='POST'>
<div id="menu">
	<input type="submit" name="action" value="NOVO">
	<input type="submit" name="action" value="GRAVAR">
	<input type="submit" name="action" value="EDITAR">
	<input type="submit" name="action" onClick="return confirm_exclusion(this);" value="<?php if ($action == "NOVO" || $action =="EDITAR") echo "DESCARTAR"; else echo "EXCLUIR"?>">
</div>

<input type="hidden" name="index" value="<?php echo $index;?>">
<input type="hidden" name="id" value="<?php echo $fields["Id"];?>">
<input type="hidden" id='last_action' name="last_action" value="<?php echo $action;?>">

<label>Nome</label><br>
<input <?php echo $readonly;?> style="width: 100%" type="text" name="nome" value="<?php echo $fields['Nome'];?>" ><br>

<label>Cargo / Atribui&ccedil;&atilde;o</label><br>
<input <?php echo $readonly;?> style="width: 100%" type="text" name="cargo" value="<?php echo $fields['Cargo'];?>" ><br>

<label>Centro / Localidade</label><br>
<input <?php echo $readonly;?> style="width: 100%" type="text" name="centro" value="<?php echo $fields['Centro'];?>" ><br>

<table  style="width: 100%;">
	<tr><td width="50%">
		<label>Telefones para contato</label><br>
		<input <?php echo $readonly;?> style="width: 100%" type="text" name="telefones" value="<?php echo $fields['Telefones'];?>" ><br>
		<label>E-mail</label><br>
		<input <?php echo $readonly;?> style="width: 100%" type="text" name="email" value="<?php echo $fields['Email'];?>" ><br>
		<label>Historico</label><br>
		<textarea <?php echo $readonly;?> rows="6" style="width: 100%" name="historico" id="historico" ><?php echo $fields['Historico'];?></textarea>
	</td><td>
		<label>Assuntos de Interesse e Pesquisa</label></span><input type="button" <?php echo $readonly;?> value="TEMAS<?php if($readonly != "")echo'(Clique em EDITAR)';?>" onClick="return new_theme();"><br>
		<textarea <?php echo $readonly;?> id="temas" name="temas" style="width: 100%; height: 100%;"><?php echo $fields['Temas'];?></textarea>
	</td></tr>
</table>

<div id="navegar">
<input <?php if(!$index) echo "disabled";?> type="submit" name="action" value="<<">
<input <?php if(!$index) echo "disabled";?> type="submit" name="action" value="<">
<input <?php if($index == $tamanho) echo "disabled";?> type="submit" name="action" value=">">
<input <?php if($index == $tamanho) echo "disabled";?> type="submit" name="action" value=">>">
</div>
<div>
</form>
<form name="procura">
	<b>Localizar Registro por nome:</b>
	<input id="campo_procura" type="text">
	<input type="submit" name="action" value="Procurar" onClick="return new_search(document.getElementById('campo_procura').value);">
</form>
</div>


<?php
	include("include/footer.inc");
?>
